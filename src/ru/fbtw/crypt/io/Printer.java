package ru.fbtw.crypt.io;

import java.io.PrintStream;
import java.util.Arrays;

public class Printer {
	public static void printSolution(int[][] matrix, PrintStream out) {
		for(int[] row : matrix){
			String string = Arrays.toString(row)
					.replace("[","")
					.replace(",","")
					.replace("]","");

			out.println(string);
		}
	}
}
