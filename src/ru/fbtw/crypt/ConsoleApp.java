package ru.fbtw.crypt;

import ru.fbtw.crypt.io.Printer;
import ru.fbtw.crypt.io.Reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

public class ConsoleApp {

	static Reader reader;

	// java Cons... input.txt output.txt
	public static void main(String[] args) {
		if (args.length == 2) {
			try {
				reader = new Reader(args[0]);

				int [][] matrix = new int[][]{
						{3, 0 ,1},
						{0,0 ,0},
						{3 ,0, 2},
						{3 ,0, 3} // int[3]
				};

				matrix = removeZeroRows(matrix);
				matrix = removeZeroColumns(matrix);

				Printer.printSolution(matrix, new PrintStream(new File("output.txt")));

			} catch (FileNotFoundException e) {
				System.err.println("Нет файла");
			}
		} else {
			System.err.println("Нет аргументов");
		}
	}



	/*
		 // int[4][]


	*/
	public static int[][] removeZeroRows(int[][] input) {

		ArrayList<Integer> zeroRows = new ArrayList<>();

		for (int i = 0; i < input.length; i++) {
			boolean isZeroRow = true;
			for (int j = 0; j < input[0].length; j++) {
				isZeroRow &= input[i][j] == 0;
			}

			if(isZeroRow){
				zeroRows.add(i);
			}

		}

		return removeRows(zeroRows, input);
	}

	public static int[][] removeZeroColumns(int[][] input) {

		ArrayList<Integer> zeroColumns = new ArrayList<>();

		for (int i = 0; i < input[0].length; i++) {
			boolean isZeroRow = true;
			for (int j = 0; j < input.length; j++) {
				isZeroRow &= input[j][i] == 0;
			}

			if(isZeroRow){
				zeroColumns.add(i);
			}

		}

		return removeColumns(zeroColumns, input);
	}

	private static int[][] removeRows(ArrayList<Integer> zeroRows, int[][] input) {
		int newRowsCount = input.length - zeroRows.size();
		int [][] result = new int[newRowsCount][];
		int count = 0;
 		for (int i = 0; i < input.length; i++) {
			if(!zeroRows.contains(i)){
				result[count] = input[i];
				count ++;
			}
		}

 		return result;
	}

	private static int[][] removeColumns(ArrayList<Integer> zeroColumns, int[][] input) {
		int newColumnsCount = input.length - zeroColumns.size();
		int [][] result = new int[input.length][newColumnsCount];
		int count = 0;
 		for (int i = 0; i < input[0].length; i++) {

			if(!zeroColumns.contains(i)){
				for(int j = 0; j < input.length; j ++){
					result[j][count] = input[j][i];
				}
				count++;
			}
		}

 		return result;
	}


}
